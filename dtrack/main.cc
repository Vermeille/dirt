/**
 * Simple shape detector program.
 * It loads an image and tries to find simple shapes (rectangle, triangle, circle, etc) in it.
 * This program is a modified version of `squares.cpp` found in the OpenCV sample dir.
 */
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <cmath>
#include <iostream>


/**
 * Helper function to display text in the center of a contour
 */
void setLabel(cv::Mat& im, const std::string label, std::vector<cv::Point>& contour)
{
   int fontface = cv::FONT_HERSHEY_SIMPLEX;
   double scale = 0.4;
   int thickness = 1;
   int baseline = 0;

   cv::Size text = cv::getTextSize(label, fontface, scale, thickness, &baseline);
   cv::Rect r = cv::boundingRect(contour);

   cv::Point pt(r.x + ((r.width - text.width) / 2), r.y + ((r.height + text.height) / 2));
   cv::rectangle(im, pt + cv::Point(0, baseline), pt + cv::Point(text.width, -text.height), CV_RGB(255,255,255), CV_FILLED);
   cv::putText(im, label, pt, fontface, scale, CV_RGB(0,0,0), thickness, 8);
}

int do_it(const cv::Mat& src)
{

   // Convert to grayscale
   cv::Mat gray;
   cv::cvtColor(src, gray, CV_BGR2GRAY);

   cv::Mat bw;
   cv::threshold(gray, bw, 150.0, 255.0, CV_THRESH_BINARY_INV);
   cv::imshow("thres",bw);
   //cv::waitKey(0);

   // Find contours
   std::vector<std::vector<cv::Point> > contours;
   cv::findContours(bw.clone(), contours, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE);

   std::vector<cv::Point> approx;
   cv::Mat dst = src.clone();

   for (int i = 0; i < contours.size(); i++)
   {
      // Approximate contour with accuracy proportional
      // to the contour perimeter
      cv::approxPolyDP(cv::Mat(contours[i]), approx, cv::arcLength(cv::Mat(contours[i]), true)*0.02, true);

      if (approx.size() == 4)
      {
         // Skip small or non-convex objects 
         if (std::fabs(cv::contourArea(contours[i])) < 100 || !cv::isContourConvex(approx))
            continue;

         setLabel(dst, "RECT", contours[i]);
         cv::Mat quad = cv::Mat::zeros(100, 100, CV_8UC3);

         // Draw corner points
         cv::circle(dst, approx[0], 3, CV_RGB(255,0,0), 2);
         cv::circle(dst, approx[1], 3, CV_RGB(0,255,0), 2);
         cv::circle(dst, approx[2], 3, CV_RGB(0,0,255), 2);
         cv::circle(dst, approx[3], 3, CV_RGB(255,255,255), 2);

         std::vector<cv::Point2f> corners;
         corners.push_back(cv::Point2f((float)(approx[0].x) , (float)(approx[0].y) ));
         corners.push_back(cv::Point2f((float)(approx[1].x) , (float)(approx[1].y) ));
         corners.push_back(cv::Point2f((float)(approx[2].x) , (float)(approx[2].y) ));
         corners.push_back(cv::Point2f((float)(approx[3].x) , (float)(approx[3].y) ));

         std::vector<cv::Point2f> quad_pts;
         quad_pts.push_back(cv::Point2f(0, 0));
         quad_pts.push_back(cv::Point2f(quad.cols, 0));
         quad_pts.push_back(cv::Point2f(quad.cols, quad.rows));
         quad_pts.push_back(cv::Point2f(0, quad.rows));

         cv::Mat transmtx = cv::getPerspectiveTransform(corners, quad_pts);
         cv::warpPerspective(dst, quad, transmtx, quad.size());
         //cv::imshow("dst", dst);
         //cv::waitKey(0);
         //cv::imshow("dst", quad);
         //cv::waitKey(0);
      }
   }

   cv::imshow("dst", dst);
   //cv::waitKey(0);

   return 0;
}

int main() {
   cv::VideoCapture capt(0);
   if(!capt.isOpened()){
      std::cout << "Error opening video stream or file" << std::endl;
      return -1;
   }

   cv::Mat src;
   for(;;) {
      capt >> src;
      do_it(src);
      char c = (char)cv::waitKey(33);
      if( c == 27 ) break;
   }
   return 0;
}
