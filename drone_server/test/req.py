# Authors: cheron_a, sanche_g
# Little client for msgpack-rpc

#! /usr/bin/env python
import socket
import sys
import msgpackrpc

def main():
    request_server = msgpackrpc.Client(msgpackrpc.Address(u"127.0.0.1", 8000))
    if (len(sys.argv) == 3):
        res = request_server.call(sys.argv[1], sys.argv[2])
    else:
        res = request_server.call(sys.argv[1])
    print res

if (__name__ == u"__main__") :
    main()

