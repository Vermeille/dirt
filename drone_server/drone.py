#! /usr/bin/env python2
# Authors: cheron_a, sanche_g
# Python Server for Crazyflie control

import socket
import sys
import sched, time
import time
import numpy
from numpy.linalg import inv
from threading import Timer

import msgpackrpc
import cflib
from cflib.crazyflie import Crazyflie
#from cfclient.utils.logconfigreader import LogConfig

class Drone(object):
    def __init__(self, link_uri):
        # Where we want to go (Cam coords)
        self.target  = numpy.mat('0;0;135;1')
        # previous pos
        self.preverror = numpy.mat('0;0;0;1')
        self.transform = numpy.mat('1,0,0,0; 0,1,0,0; 0,0,1,135; 0,0,0,1')
        self.integral = numpy.mat('0;0;0;1')
        self.markerDetected = False
        # Drone data goals
        self.roll    = 0.0
        self.pitch   = 0.0
        self.yawrate = 0
        # thrust in percent
        self.thrust  = 0
        # Crazy radio connection
        self.link_uri = link_uri
        self._cf = Crazyflie()
        self._cf.connected.add_callback(self._connected)
        self._cf.disconnected.add_callback(self._disconnected)
        self._cf.connection_failed.add_callback(self._connection_failed)
        self._cf.connection_lost.add_callback(self._connection_lost)
        self._cf.open_link(link_uri)

        # Server conf
        self.ip = socket.gethostbyname(socket.gethostname())
        self.port = 4242

    def takeoff(self):
        print u"take off"
        Timer(0.1, self._set_thrust, [0.9]).start()
        Timer(0.5, self._set_thrust, [0.5]).start()
        Timer(2, self._set_thrust,   [0.7]).start()
        Timer(2.5, self._set_thrust, [0.7]).start()
        Timer(3, self._set_thrust,   [0]).start()

    def panic(self):
        self.roll    = 0.0
        self.pitch   = 0.0
        self.yawrate = 0
        self.thrust  = 0.0
        self._cf.commander.send_setpoint(0, 0, 0, 0)

    def read_capteur(self):
        return {
            "roll"  : self.roll,
            "pitch" : self.pitch,
            "yaw"   : self.yawrate,
            "thrust": self.thrust
            }

    def correct_pos(self):
        pass

    def say_hello(self):
        self._log(u" Hello !")

    def set_thrust(self, t):
        self.thrust = float(t)

    #used internaly to set thrust from a float value
    def _set_thrust(self, t):
        self.thrust = t

    # Received coordinates
    def send_points(self, tracked, tr):
        self.markerDetected = tracked
        self.transform = numpy.asmatrix(tr)
        #self._log("pos:\n{0}".format(self.transform))
        #self.pos = transform[3].transpose()

    # crazy radio
    def _connected(self, link_uri):
        print u"[+] Connected to %s" % link_uri
        # The definition of the logconfig can be made before connecting
#        self._lg_stab = LogConfig(name="Stabilizer", period_in_ms=10)
#        self._lg_stab.add_variable("acc.z", "float")
#        #stabilizer.roll
#        #stabilizer.pitch
#        #pm.vbat
#        #imu_tests.MPU6050
#        #pid_attitide.pitch_kd
#
#        # Adding the configuration cannot be done until a Crazyflie is
#        # connected, since we need to check that the variables we
#        # would like to log are in the TOC.
#        self._cf.log.add_config(self._lg_stab)
#        if self._lg_stab.valid:
#            # This callback will receive the data
#            self._lg_stab.data_received_cb.add_callback(self._stab_log_data)
#            # This callback will be called on errors
#            self._lg_stab.error_cb.add_callback(self._stab_log_error)
#            # Start the logging
#            self._lg_stab.start()
#        else:
#            print("Could not add logconfig since some variables are not in TOC")

    def _connection_failed(self, link_uri, msg):
        print u"[!] Connection to %s failed: %s" % (link_uri, msg)

    def _connection_lost(self, link_uri, msg):
        print u"[!] Connection to %s lost: %s" % (link_uri, msg)

    def _disconnected(self, link_uri):
        print u"[!] Disconnected from %s" % link_uri
    # crazy radio log
    def _stab_log_error(self, logconf, msg):
        """Callback from the log API when an error occurs"""
        print "Error when logging %s: %s" % (logconf.name, msg)

    def _stab_log_data(self, timestamp, data, logconf):
        """Callback froma the log API when data arrives"""
        print "[%d][%s]: %s" % (timestamp, logconf.name, data)
    # internalm functions
    def _log(self, msg):
        print (u"[+] Drone %s : %s" % (self.link_uri, msg))

    def _update(self):
        #print("[+] update {0}".format(time.time()))
        # see http://wiki.bitcraze.se/projects:crazyflie:pc_utils:pylib#sending_control_commands
        #thrust = 10000 + 50000 * self.thrust

        if self.markerDetected:# and self.transform.item((1, 3)) != 0.:
            dtarget = inv(self.transform) * self.target
            #self._log('dtarget {0}'.format(dtarget))
            error = dtarget
            self.integral += error
            derivative = (error - self.preverror) / 0.1
            output = 10 * error + 0.5 * self.integral + 10 * derivative
	    self._log(u"output1 {0}".format(output[1]))

            self.thrust = 10000 + 50000 * max(0, min(output[1][0] / 20000. + 0.3, 0.85))
            self.yawrate = 0
            self.roll= numpy.sign(-output[0]) * min(abs(output[0] / 50.), 2.5)
            self.pitch = 0#numpy.sign(output[2]) * min(abs(output[2]), 0.01)
            self.preveror = error
            #self._log("error {0}".format(error))
        else:
            self.thrust *= 0.90

        self._log(u"pitch {0} roll {1} thrust {2}".format(self.pitch,
            self.roll, int(self.thrust)))
        self._cf.commander.send_setpoint(self.roll, self.pitch, self.yawrate,
                int(self.thrust))

        # update loop every 0.1 sec
        Timer(0.2, self._update, ()).start()

def main():
    # Initialize the low-level drivers (don't list the debug drivers)
    cflib.crtp.init_drivers(enable_debug_driver=False)
    # Scan for Crazyflies and use the first one found
    print u"[+] Scanning interfaces for Crazyflies..."
    available = cflib.crtp.scan_interfaces()
    # exit if we don't find any
    if (len(available) == 0):
        print u"[!] No crazyflie found"
        return

    print u"[+] Crazyflies found:"
    for i in xrange(len(available)):
        print (u"\t{0} : {1}".format(i, available[i][0]))

    # setup the drone update loop
    # start the rpc server
    d = Drone(available[0][0])
    server = msgpackrpc.Server(d)
    server.listen(msgpackrpc.Address("127.0.0.1", 8000))
    print (u"[+] Start Drone RPC server {0} at {1}:{2}".format(d.link_uri, d.ip, d.port))
    d._update()
    server.start()

if (__name__ == u"__main__") :
    main()

