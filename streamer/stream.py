from PIL import Image
from subprocess import Popen, PIPE

fps, duration = 24, 100
#server = Popen(['ffserver', '-f', 'ffserver.conf']) 
p = Popen(['ffmpeg', 
    '-y',
    '-f', 'image2pipe',
    '-vcodec', 'mjpeg',
    '-r', '24',
    '-i', '-',
    '-vcodec', 'mpeg4',
    '-qscale', '5',
    '-r', '24',
    'http://localhost:8090/feed1.ffm'], 
    stdin=PIPE)
for i in range(fps * duration):
    im = Image.new("RGB", (300, 300), (i%255, 1, 1))
    im.save(p.stdin, 'JPEG')
p.stdin.close()
p.wait()
