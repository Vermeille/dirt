/**
 * @author Clement Lucas
 * @brief Crazyflie opencv object
**/
#ifndef CVOBJECT_H
# define CVOBJECT_H

# include <opencv2/core/core.hpp>

/** CVObject class (Also called object).
Object detected in the image. A CVObject will represent a Crazyflie.
*/
class CVObject
{
public:
    int ID;
    int	firstFrameNumber;
    int lastFrameNumber;
    int frameCount;
    int avgWidth;
    int avgHeight;
    int maxWidth;
    int maxHeight;
    int collision;
    cv::Point currentPosition;
    cv::MatND currentHist;
    cv::Rect firstRectangle;
    cv::Rect lastRectangle;
    std::vector<int> contactContours;
    cv::vector<cv::Mat> frames;
    cv::vector<cv::MatND> histograms;
};

#endif // CVOBJECT_H
