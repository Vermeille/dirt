/**
 * @author Clement Lucas
**/
#include "crinfos.hh"

CRInfos::CRInfos(QWidget *parent)
    : QWidget(parent)
{
    gridLayout = new QGridLayout;

    thrustLabel = new QLabel("Thrust", this);
    pitchLabel = new QLabel("Pitch", this);
    rollLabel = new QLabel("Roll", this);
    yawLabel = new QLabel("Yaw", this);

    thrustLine = new QLineEdit(this);
    pitchLine = new QLineEdit(this);
    rollLine = new QLineEdit(this);
    yawLine = new QLineEdit(this);

    thrustLine->setEnabled(false);
    pitchLine->setEnabled(false);
    rollLine->setEnabled(false);
    yawLine->setEnabled(false);

    thrustLine->setMaximumWidth(100);
    pitchLine->setMaximumWidth(100);
    rollLine->setMaximumWidth(100);
    yawLine->setMaximumWidth(100);

    gridLayout->addWidget(thrustLabel, 0, 0);
    gridLayout->addWidget(thrustLine, 0, 1);
    gridLayout->addWidget(pitchLabel, 1, 0);
    gridLayout->addWidget(pitchLine, 1, 1);
    gridLayout->addWidget(rollLabel, 2, 0);
    gridLayout->addWidget(rollLine, 2, 1);
    gridLayout->addWidget(yawLabel, 3, 0);
    gridLayout->addWidget(yawLine, 3, 1);

    setLayout(gridLayout);
}

CRInfos::~CRInfos()
{}

void CRInfos::setCopter(CCrazyflie *copter)
{
    cfliecopter = copter;

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateInfos()));
    timer->start();
}

void CRInfos::updateInfos()
{
    if (cfliecopter)
    {
        int thrust = cfliecopter->thrust();
        float pitch = cfliecopter->pitch();
        float roll = cfliecopter->roll();
        float yaw = cfliecopter->yaw();

        thrustLine->setText(QString::number(thrust));
        pitchLine->setText(QString::number(pitch));
        rollLine->setText(QString::number(roll));
        yawLine->setText(QString::number(yaw));
    }
}
