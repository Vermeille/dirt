/**
 * @author Clement Lucas
 * @brief 3D model
**/
#ifndef MODELS_HH
# define MODELS_HH

# include <QString>
# include <QVector>

# include <cmath>

# include "point3d.hh"

class Model
{
public:
    Model() {}
    Model(const QString &filePath);

    void render(bool wireframe = false, bool normals = false) const;

    QString fileName() const { return m_fileName; }
    int faces() const { return m_pointIndices.size() / 3; }
    int edges() const { return m_edgeIndices.size() / 2; }
    int points() const { return m_points.size(); }

private:
    QString             m_fileName;
    QVector<Point3d>    m_points;
    QVector<Point3d>    m_normals;
    QVector<int>        m_edgeIndices;
    QVector<int>        m_pointIndices;
};

#endif // MODELS_HH
