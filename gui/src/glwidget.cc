/**
 * @author Clement Lucas
**/
#include <QtWidgets>
#include <QtOpenGL>

#include <cmath>

#include "glwidget.hh"

#ifndef GL_MULTISAMPLE
# define GL_MULTISAMPLE  0x809D
#endif

GLWidget::GLWidget(QWidget *parent)
    : QGLWidget(QGLFormat(QGL::SampleBuffers), parent)
{
    animationTimer.setSingleShot(false);
    connect(&animationTimer, SIGNAL(timeout()), this, SLOT(animate()));
    animationTimer.start(10);

    model_on = false;
}

GLWidget::~GLWidget()
{
    delete cflieCopter;
}

void GLWidget::animate()
{
    update();
}

void GLWidget::loadModel(QString name)
{
    if (cflieCopter)
    {
        model = new Model(name);
        model_on = true;
    }
}

void GLWidget::unloadModel()
{
    if (cflieCopter)
    {
        model_on = false;
        delete model;
    }
}

void GLWidget::initializeGL()
{
    glEnable(GL_MULTISAMPLE);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    float fAspectRatio = ((float)sizeHint().height()) / ((float)sizeHint().width());
    glFrustum(.5, -.5, -.5 * fAspectRatio, .5 * fAspectRatio, 1, 50);
    glMatrixMode(GL_MODELVIEW);
}

QSize GLWidget::minimumSizeHint() const
{
    return QSize(400, 400);
}

QSize GLWidget::sizeHint() const
{
    return QSize(400, 400);
}


void GLWidget::paintGL()
{
    glLoadIdentity();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);

    /* Move down the z-axis. */
    glTranslatef(0.0, 0.0, -20.0);

    if (cflieCopter)
    {
        if (model_on && cflieCopter->cycle())
        {
            /* Rotate. */
            glRotatef(cflieCopter->roll() - 45.0f, 1.0, 0.0, 0.0);
            glRotatef(cflieCopter->pitch(), 0.0, 1.0, 0.0);
            glRotatef(cflieCopter->yaw(), 0.0, 0.0, 1.0);

            glEnable(GL_MULTISAMPLE);
            model->render();
            glDisable(GL_MULTISAMPLE);
        }
    }

    glFlush();
}


void GLWidget::resizeGL(int width, int height)
{
    Q_UNUSED(width);
    Q_UNUSED(height);

    glViewport(0, 0, sizeHint().width(), sizeHint().height());

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    double coef = model_on ? 1.0 : 10.0;

    glOrtho(-coef, coef, -coef, coef, 0, 50.0);

    glMatrixMode(GL_MODELVIEW);
}

void GLWidget::mousePressEvent(QMouseEvent *event)
{
    lastPos = event->pos();
}

void GLWidget::mouseMoveEvent(QMouseEvent *event)
{
    lastPos = event->pos();
}

void GLWidget::switchDisplayMode()
{
    model_on = !model_on;
    resizeGL(800, 600);
}
