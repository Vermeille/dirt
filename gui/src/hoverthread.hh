/**
 * @author Clement Lucas
 * @brief Test thread to commande the crazyflie
**/
#ifndef HOVERTHREAD_HH
# define HOVERTHREAD_HH

# include <QRunnable>
# include "CCrazyflie.hh"

// Hover mode thread
class HoverThread : public QRunnable
{
public:
    HoverThread(CCrazyflie* copter_)
        : copter(copter_)
    {
        isHover.store(false);
    }

    void setHover(bool v) { isHover.store(v); }

private:
    void run()
    {
        while (copter && copter->cycle())
            copter->setThrust(isHover.load() ? 40000 : 0);
    }

private:
    CCrazyflie*         copter = NULL;
    std::atomic_bool    isHover;
};

#endif // HOVERTHREAD_HH
