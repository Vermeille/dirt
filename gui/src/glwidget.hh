/**
 * @author Clement Lucas
 * @brief 3D visualization widget
**/
#ifndef GLWIDGET_HH
# define GLWIDGET_HH

#include <QGLWidget>
#include <QTimer>

#include "CCrazyflie.hh"
#include "models.hh"

class GLWidget : public QGLWidget
{
    Q_OBJECT
public:
    GLWidget(QWidget *parent = 0);
    ~GLWidget();

    void setCopter(CCrazyflie* cflieCopter_) { cflieCopter = cflieCopter_; }

    QSize minimumSizeHint() const;
    QSize sizeHint() const;

    void loadModel(QString name);
    void unloadModel();
    void switchDisplayMode();

private slots:
    void animate();

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

private:
    CCrazyflie*     cflieCopter = NULL;
    QPoint          lastPos;
    bool            bGoon;
    QTimer          animationTimer;
    Model*          model = NULL;
    bool            model_on;
};

#endif // GLWIDGET_HH
