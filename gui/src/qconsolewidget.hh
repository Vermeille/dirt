/**
 * @author Clement Lucas
 * @brief Qt console widget
**/
#ifndef QCONSOLEWIDGET_H
# define QCONSOLEWIDGET_H

# include <QTextEdit>
# include <QDir>

class QConsoleWidget : public QTextEdit
{
    Q_OBJECT
public:
    QConsoleWidget(QWidget *parent = 0);
    ~QConsoleWidget();
private:
    int fixedPosition;

protected:
    void keyPressEvent (QKeyEvent * event);

public slots:
    void write(QString szOutput);
    void error(QString szOutput);
    void cursorPositionChanged();

private:
    void execCmd(QString cmd);

signals:
    void emitCmd(QString cmd);
};

#endif // QCONSOLEWIDGET_H
