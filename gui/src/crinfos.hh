/**
 * @author Clement Lucas
 * @brief Crazyflie informations tab widget
**/
#ifndef CRINFOS_HH
# define CRINFOS_HH

# include "CCrazyflie.hh"

# include <QtWidgets>

class CRInfos : public QWidget
{
    Q_OBJECT
public:
    explicit CRInfos(QWidget *parent = 0);
    ~CRInfos();

    void setCopter(CCrazyflie* copter);

signals:
    
private slots:
    void    updateInfos();

private:
    CCrazyflie*     cfliecopter;
    QGridLayout*    gridLayout;

    // Infos widgets
    QLabel*         thrustLabel;
    QLabel*         pitchLabel;
    QLabel*         rollLabel;
    QLabel*         yawLabel;

    QLineEdit*      thrustLine;
    QLineEdit*      pitchLine;
    QLineEdit*      rollLine;
    QLineEdit*      yawLine;

    QTimer*         timer;
};

#endif // CRINFOS_HH
