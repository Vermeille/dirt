/**
 * @author Clement Lucas
**/
#include "imageformat.hh"
#include <QColor>

QImage ImageFormat::Mat2QImage(cv::Mat const& src)
{
    cv::Mat temp;
//    cv::cvtColor(src, temp, CV_BGRA2RGB);
    cv::cvtColor(src, temp, CV_YUV2BGR);
//    cv::cvtColor(src, temp, CV_BGR2RGB);
    QImage dest((uchar*) temp.data, temp.cols, temp.rows,
                temp.step, QImage::Format_RGB16);
    QImage dest2(dest);
    dest2.detach();
    return dest2;
}

QImage ImageFormat::MatGray2QImage(cv::Mat const& src)
{
    cv::Mat temp;
    cv::cvtColor(src, temp, CV_BGR2GRAY);
    cv::Mat temp_gray;
    cv::cvtColor(temp, temp_gray, CV_GRAY2RGB);

    QImage dest((uchar*)temp_gray.data, temp_gray.cols, temp_gray.rows,
                temp_gray.step, QImage::Format_RGB888);
    QImage dest2(dest);

    dest2.detach();

    return dest2;
}

cv::Mat ImageFormat::QImage2Mat(QImage const& src)
{
    cv::Mat tmp(src.height(),src.width(),CV_8UC3,(uchar*)src.bits(),src.bytesPerLine());
    cv::Mat result;
    cvtColor(tmp, result,CV_BGR2RGB);
    return result;
}
