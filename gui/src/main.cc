/**
 * @author Clement Lucas
**/
#include <QApplication>
#include <QDesktopWidget>

#include <QtGui>
#include <QGLWidget>
#include <QGraphicsView>

#include "window.hh"

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    Window window;
    window.setFixedSize(800, 800);
    window.show();

    return app.exec();
}
