/**
 * @author Clement Lucas
 **/
#include "cvwidget.hh"
#include "imageformat.hh"
#include <opencv2/calib3d/calib3d.hpp>
#include <QVBoxLayout>
#include <algorithm>
#include <QTextStream>
#include <QPushButton>
#include <QFileDialog>

#define SERVER_ADDR         "127.0.0.1"
#define SERVER_PORT         8000
#define DEFAULT_DICOFILE    "configs/out_balise.yml"

#define IS_CUBE(Id) \
    ((Id) == 172 || (Id) == 331 || (Id) == 342 || (Id) == 224)

// Sorting functions
bool sortByFrameCount(const CVObject &lhs, const CVObject &rhs)
{
    return lhs.frameCount > rhs.frameCount;
}

CVWidget::CVWidget(QWidget *parent) :
    QWidget(parent)
{
    // Initialize variables
    frameNumber = 0;
    ID = 0;

    // Connect to Msgpack-RPC server
    client = new msgpack::rpc::client(SERVER_ADDR, SERVER_PORT);

    // Initialize Qt widgets
    timer = new QTimer(this);
    QVBoxLayout* mainLayout = new QVBoxLayout(this);

    videoFrameLabel = new QLabel(this);
    videoCapture = cv::VideoCapture(0);

    if (videoCapture.isOpened())
    {
        if (D.fromFile(DEFAULT_DICOFILE))
        {
            if (D.size() == 0)
                goto error;
            aruco::HighlyReliableMarkers::loadDictionary(D);

            // Configure other parameters
            if (ThePyrDownLevel > 0)
                MDetector.pyrDown(ThePyrDownLevel);
            MDetector.setMakerDetectorFunction(aruco::HighlyReliableMarkers::detect);
            // FIXME: Find better parameters to avoid glitches
            MDetector.setThresholdParams(21, 7);
            MDetector.setCornerRefinementMethod(aruco::MarkerDetector::LINES);
            MDetector.setWarpSize((D[0].n() + 2) * 8);
            MDetector.setMinMaxSize(0.005, 0.5);

            std::cout << "camera connected !" << std::endl;

            // Timer for aruco tracking
            connect(timer, SIGNAL(timeout()), this, SLOT(detectionLoop()), Qt::DirectConnection);
            timer->start(100);

            //Capture a temporary image from the camera
            cv::Mat imgTmp;
            videoCapture >> imgTmp;

            // configure camera
            TheCameraParameters.readFromXMLFile("configs/intrinsics.yml");
            TheCameraParameters.resize(imgTmp.size());

            //Create a black image with the size as the camera output
            imgLines = cv::Mat::zeros(imgTmp.size(), CV_8UC3 );
            QImage image = ImageFormat::Mat2QImage(imgLines);
            videoFrameLabel->setPixmap(QPixmap::fromImage(image));
        }
        else
            goto error;
    }
error:
    {
        // Set initial screen
        cv::Mat src = cv::imread("resources/initialScreen.png");
        QImage image = ImageFormat::Mat2QImage(src);
        videoFrameLabel->setPixmap(QPixmap::fromImage(image));
    }

    // Set layout
    mainLayout->addWidget(videoFrameLabel);
    mainLayout->addWidget(generateCoordZone(), 0, Qt::AlignJustify);

    // Set mainLayout in main widget
    setLayout(mainLayout);
}

QWidget* CVWidget::generateCoordZone()
{
    QWidget* widget = new QWidget(this);
    QGridLayout* layout = new QGridLayout(widget);

    QLabel* xLabel = new QLabel("X:", this);
    QLabel* yLabel = new QLabel("Y:", this);
    QLabel* zLabel = new QLabel("Z", this);
    QLabel* pitchLabel = new QLabel("Pitch:", this);
    QLabel* yawLabel = new QLabel("Yaw:", this);
    QLabel* rollLabel = new QLabel("Roll", this);
    QLabel* perimeterLabel = new QLabel("Perimeter:", this);

    xLabel->setMaximumWidth(100);
    yLabel->setMaximumWidth(100);
    zLabel->setMaximumWidth(100);
    pitchLabel->setMaximumWidth(100);
    yawLabel->setMaximumWidth(100);
    rollLabel->setMaximumWidth(100);
    perimeterLabel->setMaximumWidth(100);

    xLineEdit = new QLineEdit("0", this);
    yLineEdit = new QLineEdit("0", this);
    zLineEdit = new QLineEdit("0", this);
    pitchLineEdit = new QLineEdit("0", this);
    yawLineEdit = new QLineEdit("0", this);
    rollLineEdit = new QLineEdit("0", this);
    perimeterLineEdit = new QLineEdit("0", this);

    xLineEdit->setEnabled(false);
    yLineEdit->setEnabled(false);
    zLineEdit->setEnabled(false);
    pitchLineEdit->setEnabled(false);
    yawLineEdit->setEnabled(false);
    rollLineEdit->setEnabled(false);
    perimeterLineEdit->setEnabled(false);
    xLineEdit->setMaximumWidth(100);
    yLineEdit->setMaximumWidth(100);
    zLineEdit->setMaximumWidth(100);
    pitchLineEdit->setMaximumWidth(100);
    yawLineEdit->setMaximumWidth(100);
    rollLineEdit->setMaximumWidth(100);
    perimeterLineEdit->setMaximumWidth(100);

    layout->addWidget(xLabel, 0, 0, Qt::AlignLeft);
    layout->addWidget(xLineEdit, 0, 1, Qt::AlignLeft);
    layout->addWidget(yLabel, 1, 0, Qt::AlignLeft);
    layout->addWidget(yLineEdit, 1, 1, Qt::AlignLeft);
    layout->addWidget(zLabel, 2, 0, Qt::AlignLeft);
    layout->addWidget(zLineEdit, 2, 1, Qt::AlignLeft);

    layout->addWidget(pitchLabel, 0, 2, Qt::AlignRight);
    layout->addWidget(pitchLineEdit, 0, 3, Qt::AlignRight);
    layout->addWidget(yawLabel, 1, 2, Qt::AlignRight);
    layout->addWidget(yawLineEdit, 1, 3, Qt::AlignRight);
    layout->addWidget(rollLabel, 2, 2, Qt::AlignRight);
    layout->addWidget(rollLineEdit, 2, 3, Qt::AlignRight);

    layout->addWidget(perimeterLabel, 3, 0, Qt::AlignLeft);
    layout->addWidget(perimeterLineEdit, 3, 1, Qt::AlignLeft);

    widget->setLayout(layout);

    return widget;
}

std::string CVWidget::getTransformMatrix(const cv::Mat& Rmat, const cv::Mat& Tmat)
{
    std::ostringstream oss;
    oss << Rmat.at<float>(0, 0) << " " << Rmat.at<float>(0, 1) << " " << Rmat.at<float>(0, 2) << " " << Tmat.at<float>(0,0) << ";"
        << Rmat.at<float>(1, 0) << " " << Rmat.at<float>(1, 1) << " " << Rmat.at<float>(1, 2) << " " << Tmat.at<float>(0,1) << ";"
        << Rmat.at<float>(2, 0) << " " << Rmat.at<float>(2, 1) << " " << Rmat.at<float>(2, 2) << " " << 135. << ";"
        << "0 0 0 1";
    return oss.str();
}

struct Coord
{
    double x;
    double y;
    double z;
};

void CVWidget::detectionLoop()
{
    try
    {
        static Coord cur = { 0., 0., 0.};
        static Coord prev = { 0., 0., 0.};

        // Try to grab the next frame
        if (videoCapture.grab())
        {
            // Get current frame
            videoCapture >> rawFrame;

            // Increment number of captures images
            ++frameNumber;

            // Markers detection
            MDetector.detect(rawFrame, TheMarkers,
                             TheCameraParameters, TheMarkerSize);
            // Copy current frame to a tempory frame
            rawFrame.copyTo(rawCopyFrame);

            // If some markers are found -> Update informations
            if (TheMarkers.size() > 0)// && IS_CUBE(TheMarkers[0].id))
            {
                cv::Mat Tmat = TheMarkers[0].Tvec;
                cv::Mat Rmat(3, 3, cv::DataType<float>::type);

                double x = Tmat.at<cv::Vec3f>(0,0)[0];
                double y = -Tmat.at<cv::Vec3f>(0,0)[1];
                double z = Tmat.at<cv::Vec3f>(0,0)[2];

                cur.x = x;
                cur.y = y;
                cur.z = z;

                // You need to apply cv::Rodrigues() in order to obtain angles wrt to camera coords
                cv::Rodrigues(TheMarkers[0].Rvec, Rmat);

                // Update X/Y/Z coordinates
                xLineEdit->setText(QString::number(x));
                yLineEdit->setText(QString::number(y));
                zLineEdit->setText(QString::number(z));

                // Update Pitch/Yaw/Roll
                pitchLineEdit->setText(QString::number(-atan2(Rmat.at<float>(2,0), Rmat.at<float>(2,1))));
                yawLineEdit->setText(QString::number(acos(Rmat.at<float>(2,2))));
                rollLineEdit->setText(QString::number(-atan2(Rmat.at<float>(0,2), Rmat.at<float>(1,2))));


                // Update parameter of the first marker
                perimeterLineEdit->setText(QString::number(TheMarkers[0].getPerimeter()));

                // Send transformation matrix to the server
                std::string s = getTransformMatrix(Rmat, Tmat);
                std::cout << s << std::endl;
                client->call("send_points", true, s);

                // Print marker info and draw the markers in image
                TheMarkers[0].draw(rawCopyFrame, cv::Scalar(0, 0, 255), 1);

                // If camera parameters are valid, draw axis and cube
                if (TheCameraParameters.isValid())
                {
                    aruco::CvDrawingUtils::draw3dCube(rawCopyFrame, TheMarkers[0], TheCameraParameters);
                    aruco::CvDrawingUtils::draw3dAxis(rawCopyFrame, TheMarkers[0], TheCameraParameters);
                }
            }
            else
            {
                Coord deriv =
                {
                    (cur.x - prev.x) * 0.1 + cur.x,
                    (cur.y - prev.y) * 0.1 + cur.y - 10.,
                    (cur.z - prev.z) * 0.1 + cur.z
                };

                std::ostringstream oss;
                oss << "1 0 0 " << deriv.x << ";"
                    "0 1 0 " << deriv.y << ";"
                    "0 0 1 " << deriv.z << ";"
                    "0 0 0 1";
                client->call("send_points", false, oss.str());
            }

            // Greyscale the frame for displaying
            QImage image = ImageFormat::MatGray2QImage(rawCopyFrame);
            videoFrameLabel->setPixmap(QPixmap::fromImage(image));

            prev = cur;

            return;
        }
    } catch (std::exception &ex) { std::cerr << ex.what() << std::endl; }
}

