/**
 * @author Clement Lucas
 * @brief Main window class
**/
#ifndef WINDOW_HH
# define WINDOW_HH

# include <QWidget>
# include <QTabWidget>
# include <QThreadPool>
# include <QPushButton>
# include <atomic>
# include <QCheckBox>
# include <QFuture>

# include "CCrazyflie.hh"
# include "qconsolewidget.hh"
# include "crinfos.hh"
# include "hoverthread.hh"

class GLWidget;

class Window : public QWidget
{
    Q_OBJECT
public:
    explicit Window(QWidget *parent = 0);

protected:
    void keyPressEvent(QKeyEvent *event);

private:
    QWidget*    createConnectionZone();
    QWidget*    createConfigZone();

private slots:
    // Connection slots
    void connectCR();
    void disconnectCR();
    // Configuration slots
    void fly();
    void land();
    void loadModel();
    void exitGui();

private:
    GLWidget*       glWidget = NULL;
    CCrazyRadio*    crRadio = NULL;
    CCrazyflie*     cflieCopter = NULL;
    QConsoleWidget* console = NULL;

    QWidget*        connectionWidget = NULL;
    QPushButton*    connectButton = NULL;
    QPushButton*    disconnectButton = NULL;
    QCheckBox*      connectionCheckBox = NULL;
    QWidget*        configWidget = NULL;

    QTabWidget*     tabs = NULL;
    CRInfos*        infosTab = NULL;

    QThreadPool     threadPool;
    HoverThread*    hoverThread = NULL;
    bool            isHover;
};

#endif // WINDOW_HH
