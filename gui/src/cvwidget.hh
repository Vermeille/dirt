/**
 * @author Clement Lucas
 * @brief Opencv/Aruco widget
**/
#ifndef CVWIDGET_HH
# define CVWIDGET_HH

# include <QWidget>
# include <QTimer>
# include <QLabel>
# include <iostream>
# include <aruco/aruco.h>
# include <aruco/highlyreliablemarkers.h>
# include <utility>
# include <QLineEdit>
# include "cvObject.h"

# include <opencv2/video/background_segm.hpp>
# include <opencv2/core/core.hpp>
# include <opencv2/highgui/highgui.hpp>
# include <opencv2/imgproc/imgproc.hpp>
# include <opencv2/opencv.hpp>

# include <msgpack/rpc/client.h>

# define MARKER_SIZE    5

class CVWidget : public QWidget
{
    Q_OBJECT
public:
    explicit CVWidget(QWidget *parent = 0);

private:
    QWidget*    generateCoordZone();
    cv::Scalar  getRandomColorRGB(unsigned int ID);
    std::string getTransformMatrix(const cv::Mat& Rmat, const cv::Mat& Tmat);

private slots:
    void        detectionLoop();

private:
    int                 frameNumber;
    unsigned int        ID;

    QLabel*             videoFrameLabel;

    QLineEdit*          xLineEdit;
    QLineEdit*          yLineEdit;
    QLineEdit*          zLineEdit;
    QLineEdit*          pitchLineEdit;
    QLineEdit*          yawLineEdit;
    QLineEdit*          rollLineEdit;
    QLineEdit*          perimeterLineEdit;

    cv::Mat             rawFrame;
    cv::Mat             rawCopyFrame;
    cv::Mat             foregroundFrame;
    cv::Mat             foregroundFrameBuffer;
    cv::Mat             roiFrame;
    cv::Mat             roiFrameBuffer;
    cv::Mat             hsvRoiFrame;
    cv::Mat             roiFrameMask;
    QImage              frameImage;
    QImage              frameImageDebug;
    QTimer*             timer;
    cv::VideoCapture    videoCapture;
    cv::Mat             imgLines;

    cv::BackgroundSubtractorMOG2    mog;
    cv::vector<CVObject>            cvObjects;

    // ARUCO
    float   TheMarkerSize = MARKER_SIZE;
    int     ThePyrDownLevel;
    aruco::Dictionary           D;
    aruco::MarkerDetector       MDetector;
    aruco::CameraParameters     TheCameraParameters;
    std::vector<aruco::Marker>  TheMarkers;
    cv::Mat TheInputImage, TheInputImageCopy;

    // Msgpack-RPC
    msgpack::rpc::client        *client;

    //determines the average time required for detection
    std::pair<double, double> AvrgTime;
    double  ThresParam1, ThresParam2;
    int     iThresParam1, iThresParam2;
    int     waitTime = 0;
};

#endif // CVWIDGET_HH
