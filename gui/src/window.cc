/**
 * @author Clement Lucas
**/
#include <QtWidgets>
#include <QVBoxLayout>

#include "glwidget.hh"
#include "window.hh"
#include "cvwidget.hh"
#include <QDebug>
#include <QtConcurrent/QtConcurrent>

Window::Window(QWidget *parent)
    : QWidget(parent)
{
    QVBoxLayout* windowLayout = new QVBoxLayout(this);

    tabs = new QTabWidget(this);
    tabs->setCurrentIndex(0);

    QWidget*    copterWidget = new QWidget(this);
    CVWidget*   cvwidget = new CVWidget(this);
    infosTab = new CRInfos(this);

    QVBoxLayout* mainLayout = new QVBoxLayout;

    connectionWidget = createConnectionZone();

//    glWidget = new GLWidget(this);

    configWidget = createConfigZone();

    mainLayout->addWidget(connectionWidget);
//    mainLayout->addWidget(glWidget);
    mainLayout->addWidget(configWidget);

    copterWidget->setLayout(mainLayout);

    tabs->addTab(cvwidget, tr("Tracking"));
    tabs->addTab(copterWidget, tr("Crazyflie"));
    tabs->addTab(infosTab, tr("Infos"));

    windowLayout->addWidget(tabs);
    setLayout(windowLayout);
    setWindowTitle(tr("UAV Tracking"));
}

void Window::keyPressEvent(QKeyEvent *e)
{
    switch (e->key())
    {
    case  (Qt::Key_Escape):
        close();
        break;
    default:
        QWidget::keyPressEvent(e);
        break;
    }
}

QWidget* Window::createConnectionZone()
{
    QWidget* widget = new QWidget(this);
    QHBoxLayout* layout = new QHBoxLayout;

    connectButton = new QPushButton("Connect", widget);
    disconnectButton = new QPushButton("Disconnect", widget);

    connectionCheckBox = new QCheckBox("Disconnected", widget);
    connectionCheckBox->setEnabled(false);

    connect(connectButton, SIGNAL(released()), this, SLOT(connectCR()));
    connect(disconnectButton, SIGNAL(released()), this, SLOT(disconnectCR()));

    // FIXME: Disable
    disconnectButton->setEnabled(false);

    layout->addWidget(connectButton);
    layout->addWidget(disconnectButton);
    layout->addWidget(connectionCheckBox);
    widget->setLayout(layout);

    return widget;
}

void Window::connectCR()
{
    crRadio = new CCrazyRadio("radio://0/10/250K");

    if (crRadio->startRadio())
    {
        cflieCopter = new CCrazyflie(crRadio);

        if (!cflieCopter)
        {
            QMessageBox::critical(this, "Crazyflie Error",
                                  "Crazyflie connection failed.");
            return;
        }

        cflieCopter->setSendSetpoints(true);
        cflieCopter->setThrust(0);

//        glWidget->setCopter(cflieCopter);
        infosTab->setCopter(cflieCopter);

        connectButton->setEnabled(false);
        disconnectButton->setEnabled(true);
        connectionCheckBox->setCheckState(Qt::Checked);
        connectionCheckBox->setText("Connected");

        if (!hoverThread)
        {
            hoverThread = new HoverThread(cflieCopter);
            hoverThread->setAutoDelete(true);
            threadPool.reserveThread();
            threadPool.start(hoverThread);
        }
    }
    else
        QMessageBox::critical(this, "Crazyflie Radio Error",
                              "Crazy Radio could not be started.");
}

// FIXME: Segfault
void Window::disconnectCR()
{
    threadPool.releaseThread();
    delete hoverThread;

    delete cflieCopter;
    delete crRadio;

    sleep(2);

    connectButton->setEnabled(true);
    disconnectButton->setEnabled(false);
    connectionCheckBox->setCheckState(Qt::Unchecked);
    connectionCheckBox->setText("Disconnected");
}

QWidget* Window::createConfigZone()
{
    QWidget*        widget = new QWidget(this);
    QGridLayout*    gridLayout = new QGridLayout;

    QPushButton*    flyButton = new QPushButton("Fly", widget);
    QPushButton*    landButton = new QPushButton("Land", widget);
    QPushButton*    loadModelButton = new QPushButton("Load 3D model", widget);
    QPushButton*    exitGui = new QPushButton("Exit", widget);

    connect(flyButton, SIGNAL(released()), this, SLOT(fly()));
    connect(landButton, SIGNAL(released()), this, SLOT(land()));
    connect(loadModelButton, SIGNAL(released()), this, SLOT(loadModel()));
    connect(exitGui, SIGNAL(released()), this, SLOT(exitGui()));

    gridLayout->addWidget(flyButton, 1, 0);
    gridLayout->addWidget(landButton, 1, 1);
    gridLayout->addWidget(loadModelButton, 2, 0);
    gridLayout->addWidget(exitGui, 3, 0);

    widget->setLayout(gridLayout);

    return widget;
}

void Window::fly()
{
    if (!cflieCopter)
    {
        QMessageBox::critical(this, "Crazyflie Error", "Crazyflie is not connected.");
        return;
    }

    hoverThread->setHover(true);
}

void Window::land()
{
    if (!cflieCopter)
    {
        QMessageBox::critical(this, "Crazyflie Error", "Crazyflie is not connected.");
        return;
    }
    hoverThread->setHover(false);
}

void Window::loadModel()
{
    if (!cflieCopter)
    {
        QMessageBox::critical(this, "Crazyflie Error", "Crazyflie is not connected.");
        return;
    }

    QString filename =
            QFileDialog::getOpenFileName(this, tr("Open Model"),
                                         QDir::currentPath(), tr("Obj files (*.obj)"),
                                         0, QFileDialog::DontUseNativeDialog);

    if (!filename.isNull())
        glWidget->loadModel(filename);

    return;
}

void Window::exitGui()
{
    disconnectCR();
    delete hoverThread;

    exit(EXIT_SUCCESS);
}
