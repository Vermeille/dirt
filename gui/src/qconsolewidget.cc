/**
 * @author Clement Lucas
**/
#include "qconsolewidget.hh"

#include <QKeyEvent>
#include <iostream>

QConsoleWidget::QConsoleWidget(QWidget *parent)
    : QTextEdit(parent)
{
    setUndoRedoEnabled(false);

    setTextColor(QColor("white"));

    QPalette p = this->palette();
    p.setColor(QPalette::Base, QColor("#000000"));
    this->setPalette(p);

    fixedPosition = 0;
    write("> ");
}

QConsoleWidget::~QConsoleWidget()
{}

void QConsoleWidget::write(QString szOutput)
{
    append(szOutput);
    fixedPosition = textCursor().position();
}

void QConsoleWidget::error(QString szOutput)
{
    append(szOutput);
    fixedPosition = textCursor().position();
}

void QConsoleWidget::keyPressEvent(QKeyEvent *event)
{
        bool accept;
    int key = event->key();
    if (key == Qt::Key_Backspace)
        accept = textCursor().position() > fixedPosition;
    else if (key == Qt::Key_Return)
    {
        accept = false;
        int count = toPlainText().count() - fixedPosition;
        QString cmd = toPlainText().right(count);
        cmd = cmd.replace("> ", "");
        execCmd(cmd);
        write("> ");
    }
    else if (key == Qt::Key_Up)
        accept = false;
    else
        accept = textCursor().position() >= fixedPosition;

    if (accept)
        QTextEdit::keyPressEvent(event);
}

void QConsoleWidget::cursorPositionChanged()
{
    if (textCursor().position() < fixedPosition)
        textCursor().setPosition(fixedPosition);
}

void QConsoleWidget::execCmd(QString cmd)
{
    if (cmd == "clear")
    {
        clear();
        fixedPosition = textCursor().position();
        write("> ");
    }
    else
        emit emitCmd(cmd);
}
