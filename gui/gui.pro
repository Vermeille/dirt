######################################################################
# Author: Clement Lucas
# File: Qt Pro file for Makefile generation
######################################################################

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = app
TARGET = gui
INCLUDEPATH += .

# Input
HEADERS += \
    src/window.hh \
    src/glwidget.hh \
    src/models.hh \
    src/point3d.hh \
    src/CCrazyflie.hh \
    src/CCrazyRadio.hh \
    src/CCRTPPacket.hh \
    src/CTOC.hh \
    src/redirect.hh \
    src/qconsolewidget.hh \
    src/cvimagewidget.hh \
    src/cvwidget.hh \
    src/imageformat.hh \
    src/cvObject.h \
    src/crinfos.hh \
    src/hoverthread.hh

SOURCES += src/main.cc \
    src/CCrazyflie.cc \
    src/CCrazyRadio.cc \
    src/CCRTPPacket.cc \
    src/CTOC.cc \
    src/glwidget.cc \
    src/models.cc \
    src/window.cc \
    src/redirect.cc \
    src/qconsolewidget.cc \
    src/cvwidget.cc \
    src/imageformat.cc \
    src/crinfos.cc

QT += opengl widgets
INCLUDEPATH += ./lib/
#LIBS += -L./lib/ -lcflib -L/usr/local/lib -lusb-1.0
LIBS += -I/usr/local/include/opencv -I/usr/local/include/opencv2 -I/usr/local/include/libusb-1.0  -L/usr/local/lib \
-lusb-1.0 -lpthread -lGLU -lGL -lopencv_core -lopencv_imgproc -lopencv_highgui -lopencv_video -lopencv_calib3d -laruco -lmsgpack-rpc -lmpio -lmsgpack
QMAKE_CXXFLAGS += -std=c++11 -g -ggdb
