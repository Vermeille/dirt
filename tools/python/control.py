#! /bin/env python2
"""
Simple example that connects to the first Crazyflie found
And control it with keyboard.
"""
import Tkinter as tk
import time, sys
from threading import Thread
import json

import cflib
from cflib.crazyflie import Crazyflie
from cfclient.utils.logconfigreader import LogConfig

import logging
logging.basicConfig(level=logging.ERROR)

class Controler(object):
    def __init__(self, link_uri):
        self.root = tk.Tk()
        self.root.bind_all('<Key>', self.key)

        self.thrust = 0
        self.pitch = 0
        self.roll = 0

        self._cf = Crazyflie()

        self._cf.connected.add_callback(self._connected)
        self._cf.disconnected.add_callback(self._disconnected)
        self._cf.connection_failed.add_callback(self._connection_failed)
        self._cf.connection_lost.add_callback(self._connection_lost)

        self._cf.open_link(link_uri)

        print "Connecting to %s" % link_uri

    def _connected(self, link_uri):
        """ This callback is called form the Crazyflie API when a Crazyflie
        has been connected and the TOCs have been downloaded."""
        print "Connected to %s" % link_uri

    def _connection_failed(self, link_uri, msg):
        """Callback when connection initial connection fails (i.e no Crazyflie
        at the speficied address)"""
        print "Connection to %s failed: %s" % (link_uri, msg)

    def _connection_lost(self, link_uri, msg):
        """Callback when disconnected after a connection has been made (i.e
        Crazyflie moves out of range)"""
        print "Connection to %s lost: %s" % (link_uri, msg)

    def _disconnected(self, link_uri):
        """Callback when the Crazyflie is disconnected (called in all cases)"""
        print "Disconnected from %s" % link_uri

    def key(self, event):
        if event.keysym == 'Escape':
            self._cf.close_link()
            self.root.destroy()
        elif(event.keysym == "Prior" ):
            self.thrust += 10000
        elif(event.keysym == "Next"):
            self.thrust -= 5000
            if(self.thrust < 0):
                self.thrust = 0
        elif(event.keysym == "Up"):
            self.pitch += 5
        elif(event.keysym == "Down"):
            self.pitch -= 5
        elif(event.keysym == "Left"):
            self.roll += 5
        elif(event.keysym == "Right"):
            self.roll -= 5
        print( 'Special Key %r' % event.keysym )
        print( self.pitch, self.roll, self.thrust)

    def update(self):
        self._cf.commander.send_setpoint(self.pitch, self.roll, 0, self.thrust)
        self.root.after(10, self.update)

    def loop(self):
        self.update()
        self.root.mainloop()


if __name__ == '__main__':
    # Initialize the low-level drivers (don't list the debug drivers)
    cflib.crtp.init_drivers(enable_debug_driver=False)
    # Scan for Crazyflies and use the first one found
    print "Scanning interfaces for Crazyflies..."
    available = cflib.crtp.scan_interfaces()
    print "Crazyflies found:"
    for i in available:
        print i[0]

    if len(available) > 0:
        ctrl = Controler(available[0][0])
        ctrl.loop()
    else:
        print "No Crazyflies found, cannot run example"
